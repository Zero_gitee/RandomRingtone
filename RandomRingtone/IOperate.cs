﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomRingtone
{
    public interface IOperate
    {
        /// <summary>
        /// 检查是否已授权
        /// </summary>
        /// <returns></returns>
        bool CheckPermission();

        /// <summary>
        /// 授权
        /// </summary>
        void BeginPermission();

        /// <summary>
        /// 初始化
        /// </summary>
        void Initialize();

        /// <summary>
        /// 显示提示文字
        /// </summary>
        /// <param name="msg">提示文字</param>
        void ShowMessage(string msg);
    }
}
