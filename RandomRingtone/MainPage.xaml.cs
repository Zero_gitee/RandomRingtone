﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Reflection;
using Xamarin.Essentials;

namespace RandomRingtone
{
    public partial class MainPage : ContentPage
    {
        IOperate _Operate = DependencyService.Get<IOperate>();

        public MainPage()
        {
            InitializeComponent();

            try
            {
                this.BindingContext = Action.UserData;
            }
            catch (Exception ex)
            {
                Action.WriteLog($"Exception：{ex.ToString()}");
            }
        }

        private void BtnChange_Clicked(object sender, EventArgs e)
        {
            try
            {
                var result = Action.Change(Action.UserData);
                if (result.Item1)
                {
                    Action.UserData.OperateTime = DateTime.Now;
                    Action.UserData.Operater = "手动操作";

                    Action.SaveUserData(Action.UserData);

                    _Operate.ShowMessage(result.Item2);
                }
                else
                {
                    this.DisplayAlert(
                        "操作失败",
                        result.Item2,
                        "OK");
                }
            }
            catch (Exception ex)
            {
                Action.WriteLog($"Exception：{ex.ToString()}");
                this.DisplayAlert(
                    "Exception",
                    ex.ToString(),
                    "OK");
            }
        }

        private void BtnData_Clicked(object sender, EventArgs e)
        {
            try
            {
                var str = Action.GetUserDataString();
                this.DisplayAlert(
                    "用户数据",
                    str,
                    "OK");
            }
            catch (Exception ex)
            {
                Action.WriteLog($"Exception：{ex.ToString()}");
                this.DisplayAlert(
                    "Exception",
                    ex.ToString(),
                    "OK");
            }
        }

        async private void BtnLog_Clicked(object sender, EventArgs e)
        {
            try
            {
                var str = Action.GetLogString();
                var b = await this.DisplayAlert(
                    "删除系统日志？",
                    str,
                    "确定",
                    "取消");
                if (b)
                {
                    Action.DeleteLog();
                }
            }
            catch (Exception ex)
            {
                Action.WriteLog($"Exception：{ex.ToString()}");
                await this.DisplayAlert(
                    "Exception",
                    ex.ToString(),
                    "OK");
            }
        }

        private void BtnAbout_Clicked(object sender, EventArgs e)
        {
            Browser.OpenAsync(@"https://gitee.com/Zero_gitee/RandomRingtone", BrowserLaunchMode.SystemPreferred);
        }
    }
}
