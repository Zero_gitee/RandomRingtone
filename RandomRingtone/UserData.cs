﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Newtonsoft.Json;

namespace RandomRingtone
{
    public class UserData : INotifyPropertyChanged
    {
        private string _铃外部存储根目录;
        private string _铃声文件;
        private string _铃声库;
        private string _当前铃声;
        private DateTime _OperateTime;
        private string _Operater;

        public event PropertyChangedEventHandler PropertyChanged;

        bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string 铃外部存储根目录 { get => _铃外部存储根目录; set => SetProperty(ref _铃外部存储根目录, value); }
        public string 铃声文件 { get => _铃声文件; set => SetProperty(ref _铃声文件, value); }
        public string 铃声库 { get => _铃声库; set { SetProperty(ref _铃声库, value); OnPropertyChanged("CanOperate"); OnPropertyChanged("铃声库信息"); } }
        public string 当前铃声 { get => _当前铃声; set => SetProperty(ref _当前铃声, value); }
        public DateTime OperateTime { get => _OperateTime; set { SetProperty(ref _OperateTime, value); OnPropertyChanged("OperateInfo"); OnPropertyChanged("铃声库信息"); } }
        public string Operater { get => _Operater; set { SetProperty(ref _Operater, value); OnPropertyChanged("OperateInfo"); OnPropertyChanged("铃声库信息"); } }
        [JsonIgnore]
        public string OperateInfo { get => $"上次切换：{_OperateTime.ToString("yyyy-MM-dd HH:mm:ss")}({_Operater})"; }
        [JsonIgnore]
        public bool CanOperate
        {
            get
            {
                if (Directory.Exists(_铃声库))
                {
                    var files = Directory.GetFiles(_铃声库, "*.mp3", SearchOption.AllDirectories);
                    if (files.Length > 0)
                    {
                        return true;
                    }
                }

                return false;
            }

        }

        [JsonIgnore]
        public string HintText
        {
            get
            {
                return @"使用说明：
    1、首次使用，请将您的闹钟铃声设置为‘追梦人’
    2、请在铃声库（内部存储/media/audio/alarms_铃声库/）中加入您喜欢的音乐（MP3），以后随时可以更新您的铃声库
    3、点击“立即切换”即可随机切换闹钟铃声
    4、每次开机会自动切换闹钟铃声（需要您允许本App开机启动）";
            }

        }

        [JsonIgnore]
        public string 铃声库信息
        {
            get
            {
                var count = 0;
                if (Directory.Exists(_铃声库))
                {
                    count = Directory.GetFiles(_铃声库, "*.mp3", SearchOption.AllDirectories).Length;
                }
                return $"库中铃声{count}首";
            }
        }
    }
}
