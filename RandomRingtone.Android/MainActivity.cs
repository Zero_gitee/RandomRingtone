﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content;
using System.Linq;
using Android;

namespace RandomRingtone.Droid
{
    [Activity(Label = "随机闹铃", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        static public MainActivity Activity = null;
        IOperate _Operate = new Operate();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            Activity = this;

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());

            var x = typeof(Xamarin.Forms.Themes.DarkThemeResources);
        }

        protected override void OnStart()
        {
            base.OnStart();
        }

        protected override void OnResume()
        {
            base.OnResume();

            if (_Operate.CheckPermission())
            {
                _Operate.Initialize();
            }
            else
            {
                _Operate.BeginPermission();
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            if (requestCode == 0 && permissions.Length == 1 && permissions[0] == Manifest.Permission.WriteExternalStorage)
            {
                if (grantResults[0] == Permission.Granted)
                {
                    //已授权
                    _Operate.Initialize();
                }
                else
                {
                    this.Finish();
                }
            }
        }
    }
}