﻿using Android;
using Android.Content;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Widget;
using RandomRingtone.Droid;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

[assembly: Dependency(typeof(RandomRingtone.Operate))]
namespace RandomRingtone
{
    public class Operate : IOperate
    {
        static Toast _Toast = null;

        /// <summary>
        /// 检查是否已授权
        /// </summary>
        /// <returns></returns>
        public bool CheckPermission()
        {
            return ContextCompat.CheckSelfPermission(Android.App.Application.Context, Manifest.Permission.WriteExternalStorage) == Permission.Granted;
        }

        /// <summary>
        /// 授权
        /// </summary>
        public void BeginPermission()
        {
            ActivityCompat.RequestPermissions(MainActivity.Activity, new String[] { Manifest.Permission.WriteExternalStorage }, 0);
        }

        /// <summary>
        /// 初始化
        /// </summary>
        async public void Initialize()
        {
            if (!Directory.Exists(Action.UserData.铃外部存储根目录))
            {
                Action.UserData.铃外部存储根目录 = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            }

            var alarmsPath = Path.Combine(Action.UserData.铃外部存储根目录, @"media/audio/alarms/");
            if (!Directory.Exists(alarmsPath))
            {
                Directory.CreateDirectory(alarmsPath);
            }

            var alarmsLibPath = Path.Combine(Action.UserData.铃外部存储根目录, @"media/audio/alarms_铃声库/");
            if (!Directory.Exists(alarmsLibPath))
            {
                Directory.CreateDirectory(alarmsLibPath);

                //默认铃声
                var defaultAlarm = Path.Combine(alarmsLibPath, @"追梦人.mp3");
                using (var stream = await FileSystem.OpenAppPackageFileAsync(@"defaultAlarm.mp3"))
                {
                    using (var st = File.Create(defaultAlarm))
                    {
                        stream.CopyTo(st);
                    }
                }
            }

            if (!Directory.Exists(Action.UserData.铃声库))
            {
                Action.UserData.铃声库 = alarmsLibPath;
            }

            if (!File.Exists(Action.UserData.铃声文件))
            {
                //默认铃声
                var defaultAlarm = Path.Combine(alarmsPath, @"追梦人.mp3");
                using (var stream = await FileSystem.OpenAppPackageFileAsync(@"defaultAlarm.mp3"))
                {
                    using (var st = File.Create(defaultAlarm))
                    {
                        stream.CopyTo(st);
                    }
                }

                Action.UserData.铃声文件 = defaultAlarm;
            }

            //用于强制刷新
            Action.UserData.铃声库 = Action.UserData.铃声库;
        }


        /// <summary>
        /// 显示提示文字
        /// </summary>
        /// <param name="msg">提示文字</param>
        public void ShowMessage(string msg)
        {
            if (_Toast == null)
            {
                Android.Widget.Toast.MakeText(Android.App.Application.Context, msg, ToastLength.Short).Show();
            }
            else
            {
                _Toast.SetText(msg);
                _Toast.Show();
            }
        }
    }
}
