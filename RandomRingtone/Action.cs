﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace RandomRingtone
{
    public class Action
    {
        IOperate _Operate = DependencyService.Get<IOperate>();

        static public UserData UserData { get; set; }

        static public string _AppRootPath { get; private set; }

        static Action()
        {
            UserData = new UserData();
            try
            {
                _AppRootPath = FileSystem.AppDataDirectory;
                var fileName = Path.Combine(_AppRootPath, "UserDatas.txt");
                if (File.Exists(fileName))
                {
                    var strJson = File.ReadAllText(fileName, Encoding.UTF8);
                    //WriteLog($"用户数据：{strJson}");
                    UserData = JsonConvert.DeserializeObject<UserData>(strJson);
                }
            }
            catch (Exception e)
            {
                WriteLog($"读取用户数据异常：{e.ToString()}");
                UserData = new UserData();
            }
        }

        static public void WriteLog(string contents)
        {
            try
            {
                var logFile = Path.Combine(_AppRootPath, "RandomRingtone.log");
                var log = string.Format(@"【{0}】
{1}
", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), contents);

                File.AppendAllText(logFile, log, Encoding.UTF8);
            }
            catch
            {
                //WriteLog($"WriteLog异常：{e.ToString()}");
            }
        }

        static public Tuple<bool, string> Change(UserData userData)
        {
            //var logFile = Path.Combine(_AppRootPath, string.Format("{0}.log", DateTime.Now.ToString("yyyy-MM-dd")));

            //WriteLog("开始转换");

            var ringtoneFile = userData.铃声文件;
            //WriteLog($"铃声文件：{ringtoneFile}");

            var ringtonePath = userData.铃声库;
            //WriteLog($"铃声文件库：{ringtoneFile}");
            if (!Directory.Exists(ringtonePath))
            {
                var err = $"【操作失败】铃声库'{ringtonePath}'不存在。";
                //WriteLog(err);
                return new Tuple<bool, string>(false, err);
            }

            var files = Directory.GetFiles(ringtonePath, "*.mp3", SearchOption.AllDirectories);
            //WriteLog($"在铃声文件库中找到{files.Length}首mp3。");
            if (files.Length == 0)
            {
                var err = "【操作失败】铃声文件库中无文件。";
                WriteLog(err);
                return new Tuple<bool, string>(false, err);
            }

            var random = new Random();
            var newIndex = random.Next(0, files.Length - 1);
            var newFile = files[newIndex];
            //WriteLog($"随机选择新铃声文件：{newFile}");

            File.Copy(newFile, ringtoneFile, true);
            userData.当前铃声 = Path.GetFileName(newFile);
            var msg = $"铃声已切换：{userData.当前铃声}。";
            //WriteLog(msg);
            return new Tuple<bool, string>(true, msg);
        }

        static public void SaveUserData(UserData userData)
        {
            var fileName = Path.Combine(_AppRootPath, "UserDatas.txt");
            File.Delete(fileName);

            IsoDateTimeConverter timeConverter = new IsoDateTimeConverter
            {
                DateTimeFormat = "yyyy-MM-dd HH:mm:ss"
            };
            string strJson = JsonConvert.SerializeObject(userData, Formatting.Indented, timeConverter);
            File.WriteAllText(fileName, strJson, Encoding.UTF8);
        }

        static public string GetUserDataString()
        {
            var fileName = Path.Combine(_AppRootPath, "UserDatas.txt");
            if (File.Exists(fileName))
            {
                return File.ReadAllText(fileName, Encoding.UTF8);
            }
            else
            {
                return string.Empty;
            }
        }

        static public string GetLogString()
        {
            var logFile = Path.Combine(_AppRootPath, "RandomRingtone.log");
            if (File.Exists(logFile))
            {
                return File.ReadAllText(logFile, Encoding.UTF8);
            }
            else
            {
                return string.Empty;
            }
        }

        static public void DeleteLog()
        {
            var logFile = Path.Combine(_AppRootPath, "RandomRingtone.log");
            File.Delete(logFile);
        }
    }
}
