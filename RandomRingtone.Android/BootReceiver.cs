﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RandomRingtone.Droid
{
    [BroadcastReceiver(Enabled = true, Exported = true)]
    //[IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted, Android.Content.Intent.ActionUserUnlocked, Android.Content.Intent.ActionUserInitialize, Android.Content.Intent.ActionShutdown })]
    [IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted })]
    public class BootReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            try
            {
                if (intent != null)
                {
                    Action.WriteLog($"接收系统广播：{intent.Action}");
                }
                else
                {
                    Action.WriteLog("intent == null");
                }

                var result = Action.Change(Action.UserData);
                if (result.Item1)
                {
                    Action.UserData.OperateTime = DateTime.Now;
                    Action.UserData.Operater = "自动切换";
                    Action.SaveUserData(Action.UserData);
                    //Action.WriteLog($"操作成功：{result.Item2}");
                }
                else
                {
                    Action.WriteLog($"System操作失败：{result.Item2}");
                }
            }
            catch (Exception ex)
            {
                Action.WriteLog($"Exception：{ex.ToString()}");
            }
        }
    }
}